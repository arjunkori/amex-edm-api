package com.bdiplus.edmapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmexEdmApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmexEdmApiApplication.class, args);
	}

}

