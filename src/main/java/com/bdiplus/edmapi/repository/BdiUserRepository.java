package com.bdiplus.edmapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bdiplus.edmapi.model.BdiUser;

@Repository
public interface BdiUserRepository extends JpaRepository<BdiUser,Integer > {

}
