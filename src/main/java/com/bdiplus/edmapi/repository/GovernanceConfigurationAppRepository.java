package com.bdiplus.edmapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bdiplus.edmapi.model.GovernanceConfigurationApp;



public interface GovernanceConfigurationAppRepository extends JpaRepository<GovernanceConfigurationApp, Integer> {

	@Query(nativeQuery = true, value = "select * from bdi_tbl_11 where bdi_attrib_120 =?1")
	List<GovernanceConfigurationApp> findAll(int bdi_product_type);
	
	GovernanceConfigurationApp save(GovernanceConfigurationApp governanceConfigurationApp);
	
	@Modifying
	@Query(nativeQuery = true, value = "Update bdi_tbl_11 set bdi_attrib_34=:approval_reqd_flg where bdi_attrib_30=:bdiapp_id")
	void updateApprovalFlg(@Param("approval_reqd_flg") Integer approvalReqdFlg, @Param("bdiapp_id") Integer bdiappId);
	
	void delete(GovernanceConfigurationApp governanceConfigurationApp);
   //GovernanceConfigurationApp findByDataAppId(int id);
	
}
