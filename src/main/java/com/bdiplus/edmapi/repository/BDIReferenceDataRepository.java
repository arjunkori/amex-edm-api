package com.bdiplus.edmapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bdiplus.edmapi.model.BDIReferenceData;

@Transactional
@Repository

public interface BDIReferenceDataRepository extends JpaRepository<BDIReferenceData, Integer> {

	BDIReferenceData save(BDIReferenceData bDIReferenceData);
	
	List<BDIReferenceData> findAll();
	
	List<BDIReferenceData> findByTableNameAndColumnName(String tableName, String columnName);
	
//	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1800= :table_nm and bdi_attrib_1801 :column_nm and ref_val :ref_val")
//	List<BDIReferenceData> findByTableNameAndColumnNameAndRefValue(@Param("table_nm") String table_nm, @Param("column_nm") String column_nm, @Param("ref_val") String ref_val);
	List<BDIReferenceData> findByTableNameAndColumnNameAndRefValue(String tableName, String columnName, String refValue);
	
	@Query(nativeQuery = true, value="call bdipp_get_storage_format()")
	List<BDIReferenceData> getStorageFormat();
	
	@Query(nativeQuery = true, value="call bdipp_get_data_format()")
	List<BDIReferenceData> getDataFormat();
	
	@Query(nativeQuery = true, value="call get_ingestion_di_data_filter_timeframe_unit()")
	List<BDIReferenceData> getDataIngestionTimeFrameunit();
	
	List<BDIReferenceData> findByColumnNameOrderByDisplayOrderAsc(String columnName);
	
	@Query(nativeQuery = true, value="select *  from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_1756'")
	List<BDIReferenceData> getTimeFilterUnit();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_947' order by bdi_attrib_1802 asc")
	List<BDIReferenceData> getAlgorithmList();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_950' order by bdi_attrib_1802 asc")
	List<BDIReferenceData> getProcessDropdownList();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_950' order by bdi_attrib_1802 asc")
	List<BDIReferenceData> getUCFrequncyLevel();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_453' order by bdi_attrib_1802 asc")
	List<BDIReferenceData> getUCFrequncyList();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_1191' order by bdi_attrib_1802 asc")
	List<BDIReferenceData> getOutputDesFlag();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_1104' AND bdi_attrib_1800='bdi_tbl_49'")
	List<BDIReferenceData> getOutputFileFormat();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_1107'  order by bdi_attrib_1802 asc")
	List<BDIReferenceData> getOutputStorageFormatList();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_1365' AND bdi_attrib_1800='bdi_tbl_53'  order by bdi_attrib_1802 asc")
	List<BDIReferenceData> getOutputLayout();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_1365' AND bdi_attrib_1800='bdi_tbl_54'  order by bdi_attrib_1802 asc")
	List<BDIReferenceData> getOutputAttributeTypeList();
	
	/*@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1801='column_data_type'  order by display_order asc")
	List<BDIReferenceData> getQCProgram();*/
	
	@Query(nativeQuery = true, value="select bdi_attrib_1803,bdi_attrib_1804 from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_30' AND bdi_attrib_1801='bdi_attrib_448'")
	List getDonutGraph();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_71' and bdi_attrib_1801='bdi_attrib_1727'")
	List<BDIReferenceData> getDiIngestionType();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_71' and bdi_attrib_1801='bdi_attrib_1733'")
	List<BDIReferenceData> getDiTriggerType();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_71' and bdi_attrib_1801='bdi_attrib_1729'")
	List<BDIReferenceData> getDiDataRefreshType();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_71' and bdi_attrib_1801='bdi_attrib_1715'")
	List<BDIReferenceData> getDiFreq();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_71' and bdi_attrib_1801='bdi_attrib_1731'")
	List<BDIReferenceData> getDiRefreshKeyType();
	
	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_71'")
	List<BDIReferenceData> getAllBdidbInstTables();

	@Query(nativeQuery = true, value="select * from bdi_tbl_420 where bdi_attrib_1800= :table_nm and bdi_attrib_1801 =:column_nm and bdi_attrib_1803 =:ref_val")
	List<BDIReferenceData> getBDIReferenceData(@Param("table_nm") String table_nm, @Param("column_nm") String column_nm, @Param("ref_val") Integer ref_val);

	@Query(nativeQuery = true, value="select *from bdi_tbl_420  where bdi_attrib_1801='bdi_attrib_1109'  order by bdi_attrib_1802 asc")
	List<BDIReferenceData> getRadioHeadersFlag();
	
	@Query(nativeQuery = true, value="select *from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_71' and bdi_attrib_1801='bdi_attrib_1713'")
	List<BDIReferenceData> getDiCurrentStatus();
	
	@Query(nativeQuery = true, value="select *from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_69' and bdi_attrib_1801='bdi_attrib_1683'")
	List<BDIReferenceData> getDiRegStatus();
	
	@Query(nativeQuery = true, value="select *from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_49' and bdi_attrib_1801='bdi_attrib_1108'")
	List<BDIReferenceData> getColumnDelimiter();
	
	@Query(nativeQuery = true, value="select *from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_49' and bdi_attrib_1801='bdi_attrib_1106'")
	List<BDIReferenceData> getFileLayoutType();
	
	@Query(nativeQuery = true, value="select *from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_49' and bdi_attrib_1801='bdi_attrib_1109'")
	List<BDIReferenceData> getFileHasAHeaderFlg();
	
	@Query(nativeQuery = true, value="select *from bdi_tbl_420 where bdi_attrib_1800='bdi_tbl_28' and bdi_attrib_1801='bdi_attrib_423'")
	List<BDIReferenceData> getGovApprovalLogApprovalType();
	
	@Query(nativeQuery = true, value="select *  from bdi_tbl_420 where bdi_attrib_1801='bdi_attrib_1729'")
	List<BDIReferenceData> getLoadTypeData();
	
}
