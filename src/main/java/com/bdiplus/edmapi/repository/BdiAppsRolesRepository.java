package com.bdiplus.edmapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdiplus.edmapi.model.BdiAppsRoles;
import com.bdiplus.edmapi.model.BdiAppsRolesKey;

public interface BdiAppsRolesRepository extends JpaRepository<BdiAppsRoles, BdiAppsRolesKey> {

	BdiAppsRoles save(BdiAppsRoles bdiAppsRoles);
	
	void delete(BdiAppsRoles bdiAppsRoles);
	
	void deleteByBdiappRole(short role);
	
}
