package com.bdiplus.edmapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdiplus.edmapi.model.GovernanceConfigurationAppRole;

public interface GovernanceConfigurationAppRoleRepository extends JpaRepository<GovernanceConfigurationAppRole, Integer> {

	GovernanceConfigurationAppRole save(GovernanceConfigurationAppRole governanceConfigurationAppRole);
	
}
