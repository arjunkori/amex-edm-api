package com.bdiplus.edmapi.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bdiplus.edmapi.model.GovernanceConfigurationApprovalFlow;
import com.bdiplus.edmapi.model.GovernanceConfigurationApprovalFlowKey;

public interface GovernanceConfigurationApprovalFlowRepository extends JpaRepository<GovernanceConfigurationApprovalFlow, GovernanceConfigurationApprovalFlowKey> {

	List<GovernanceConfigurationApprovalFlow> findByApprovalType(Byte approvalType);
	
	GovernanceConfigurationApprovalFlow save(GovernanceConfigurationApprovalFlow governanceConfigurationApprovalFlow);
	
	long count();
	
	@Query(nativeQuery = true, value = "select max(bdi_attrib_400) from bdi_tbl_27")
	Integer findTop1ByGovApplId();
	
	void deleteByGovApplId(Integer gaid);
	
	GovernanceConfigurationApprovalFlow findByGovApplId(int id);
	
	// my created
	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "delete from bdi_tbl_27 where bdi_attrib_400=:gov_appl_id")
	void deleteUsingGovApplId(@Param("gov_appl_id") Integer gov_appl_id);
	
	
}
