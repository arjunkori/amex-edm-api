package com.bdiplus.edmapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdiplus.edmapi.model.GovernanceConfigurationDataAllowed;


public interface GovernanceConfigurationDataAllowedRepository extends JpaRepository<GovernanceConfigurationDataAllowed, Short>{

	List<GovernanceConfigurationDataAllowed> findAll();
	
	GovernanceConfigurationDataAllowed save(GovernanceConfigurationDataAllowed governanceConfigurationDataAllowed);
	
	void delete(GovernanceConfigurationDataAllowed governanceConfigurationDataAllowed);
	
	GovernanceConfigurationDataAllowed findByDataAttributeId(short id);
	
}
