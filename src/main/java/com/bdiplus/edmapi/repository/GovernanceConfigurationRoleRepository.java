package com.bdiplus.edmapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bdiplus.edmapi.model.GovernanceConfigurationRole;

public interface GovernanceConfigurationRoleRepository extends JpaRepository<GovernanceConfigurationRole, Short> {

	GovernanceConfigurationRole save(GovernanceConfigurationRole governanceConfigurationRole);
	
	Short deleteByBdiappRole(Short role);
	
	/*@Query(nativeQuery = true, value = "select rs.bdi_attrib_10 AS bdiapp_role,rs.bdi_attrib_11 AS bdiapp_role_nm, rs.bdi_attrib_12 AS first_approver_nm, rs.bdi_attrib_13 AS first_approver_email,rs.bdi_attrib_14 AS second_approver_nm,rs.bdi_attrib_15 AS second_approver_email,rs.bdi_attrib_16 AS bdiapp_role_cat,GROUP_CONCAT(rs.bdi_attrib_30 SEPARATOR ',') AS bdiapp_id,GROUP_CONCAT(app.bdi_attrib_10 SEPARATOR ',') AS app_nm from (select p.bdi_attrib_10,p.bdi_attrib_11, p.bdi_attrib_12, p.bdi_attrib_13,p.bdi_attrib_14,p.bdi_attrib_15,p.bdi_attrib_16,r.bdi_attrib_30 from bdi_tbl_10 p inner join bdi_tbl_12 r where p.bdi_attrib_10 = r.bdi_attrib_10) rs inner join bdi_tbl_11 app where rs.bdi_attrib_30 = app.bdi_attrib_30 GROUP BY rs.bdi_attrib_10")
	List getAllData();*/
	
	@Query(nativeQuery = true, value = "select rs.bdi_attrib_10 AS bdiapp_role,rs.bdi_attrib_11 AS bdiapp_role_nm, rs.bdi_attrib_12 AS first_approver_nm, rs.bdi_attrib_13 AS first_approver_email,rs.bdi_attrib_14 AS second_approver_nm,rs.bdi_attrib_15 AS second_approver_email,rs.bdi_attrib_16 AS bdiapp_role_cat,GROUP_CONCAT(rs.bdi_attrib_30 SEPARATOR ',') AS bdiapp_id,GROUP_CONCAT(app.bdi_attrib_31 SEPARATOR ',') AS app_nm from (select p.bdi_attrib_10,p.bdi_attrib_11, p.bdi_attrib_12, p.bdi_attrib_13,p.bdi_attrib_14,p.bdi_attrib_15,p.bdi_attrib_16,r.bdi_attrib_30 from bdi_tbl_10 p inner join bdi_tbl_12 r where p.bdi_attrib_10 = r.bdi_attrib_10) rs inner join bdi_tbl_11 app where rs.bdi_attrib_30 = app.bdi_attrib_30 GROUP BY rs.bdi_attrib_10")
	List getAllData();
	
	//changed r.bdi_attrib_30 to r.bdi_attrib_47
	//changed rs.bdi_attrib_30 to rs.bdi_attrib_47
	
	
	
	@Query(nativeQuery = true, value = "select rs.bdi_attrib_10 AS bdiapp_role,rs.bdi_attrib_11 AS bdiapp_role_nm, rs.bdi_attrib_12 AS first_approver_nm, rs.bdi_attrib_13 AS first_approver_email,rs.bdi_attrib_14 AS second_approver_nm,rs.bdi_attrib_15 AS second_approver_email,rs.bdi_attrib_16 AS bdiapp_role_cat,GROUP_CONCAT(rs.bdi_attrib_30 SEPARATOR ',') AS bdiapp_id,GROUP_CONCAT(app.bdi_attrib_10 SEPARATOR ',') AS app_nm from (select p.bdi_attrib_10,p.bdi_attrib_11, p.bdi_attrib_12, p.bdi_attrib_13,p.bdi_attrib_14,p.bdi_attrib_15,p.bdi_attrib_16,r.bdi_attrib_30 from bdi_tbl_10 p inner join bdi_tbl_12 r where p.bdi_attrib_10 = r.bdi_attrib_10) rs inner join bdi_tbl_11 app where rs.bdi_attrib_30 = app.bdi_attrib_30 GROUP BY rs.bdi_attrib_10")
	List userRoles();
	
	List<GovernanceConfigurationRole> findByBdiappRole(int user_role);
	
	List<GovernanceConfigurationRole> findAll();
}
