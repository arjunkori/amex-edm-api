package com.bdiplus.edmapi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;


	
	@Entity
	@Table(name="bdi_tbl_24")
	public class GovernanceConfigurationDataAllowed {
		@Id
		@Column(name="bdi_attrib_310",columnDefinition = "SMALLINT")
		@Type(type = "org.hibernate.type.ShortType")
		private short dataAttributeId;
		
		@Column(name="bdi_attrib_311",columnDefinition = "SMALLINT")
		@Type(type = "org.hibernate.type.ShortType")
		private short data_sensitive_type;
		
		@Column(name = "bdi_attrib_312")
		private String data_sensitive_nm;

		@Column(name = "bdi_attrib_313")
		private String data_attribute_nm;
		
		@Column(name = "bdi_attrib_314")
		private String data_attribute_desc;
		
		@Column(name = "bdi_attrib_315")
		private String retention_period_unit;
		
		@Column(name="bdi_attrib_316",columnDefinition = "SMALLINT")
		@Type(type = "org.hibernate.type.ShortType")
		private short retention_period;
		
		@Column(name = "bdi_attrib_317", columnDefinition = "TINYINT")
		private byte masking_reqd_flg;
		
		@Column(name = "bdi_attrib_318", columnDefinition = "TINYINT")
		private byte encryption_reqd_flg;
		
		@Column(name = "bdi_attrib_319", columnDefinition = "TINYINT")
		private byte approval_flg;
		
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
		@Column(name = "bdi_attrib_320", columnDefinition="DATETIME")
		@Temporal(TemporalType.TIMESTAMP)
		private Date updt_dt;
		
		@Column(name = "bdi_attrib_47")
		private int updt_by_id;

		public short getDataAttributeId() {
			return dataAttributeId;
		}

		public void setDataAttributeId(short dataAttributeId) {
			this.dataAttributeId = dataAttributeId;
		}

		public short getData_sensitive_type() {
			return data_sensitive_type;
		}

		public void setData_sensitive_type(short data_sensitive_type) {
			this.data_sensitive_type = data_sensitive_type;
		}

		public String getData_sensitive_nm() {
			return data_sensitive_nm;
		}

		public void setData_sensitive_nm(String data_sensitive_nm) {
			this.data_sensitive_nm = data_sensitive_nm;
		}

		public String getData_attribute_nm() {
			return data_attribute_nm;
		}

		public void setData_attribute_nm(String data_attribute_nm) {
			this.data_attribute_nm = data_attribute_nm;
		}

		public String getData_attribute_desc() {
			return data_attribute_desc;
		}

		public void setData_attribute_desc(String data_attribute_desc) {
			this.data_attribute_desc = data_attribute_desc;
		}

		public String getRetention_period_unit() {
			return retention_period_unit;
		}

		public void setRetention_period_unit(String retention_period_unit) {
			this.retention_period_unit = retention_period_unit;
		}

		public short getRetention_period() {
			return retention_period;
		}

		public void setRetention_period(short retention_period) {
			this.retention_period = retention_period;
		}

		public byte getMasking_reqd_flg() {
			return masking_reqd_flg;
		}

		public void setMasking_reqd_flg(byte masking_reqd_flg) {
			this.masking_reqd_flg = masking_reqd_flg;
		}

		public byte getEncryption_reqd_flg() {
			return encryption_reqd_flg;
		}

		public void setEncryption_reqd_flg(byte encryption_reqd_flg) {
			this.encryption_reqd_flg = encryption_reqd_flg;
		}

		public byte getApproval_flg() {
			return approval_flg;
		}

		public void setApproval_flg(byte approval_flg) {
			this.approval_flg = approval_flg;
		}

		public Date getUpdt_dt() {
			return updt_dt;
		}

		public void setUpdt_dt(Date updt_dt) {
			this.updt_dt = updt_dt;
		}

		public int getUpdt_by_id() {
			return updt_by_id;
		}

		public void setUpdt_by_id(int updt_by_id) {
			this.updt_by_id = updt_by_id;
		}

	}



