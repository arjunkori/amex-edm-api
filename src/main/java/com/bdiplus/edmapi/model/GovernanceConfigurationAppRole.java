package com.bdiplus.edmapi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "bdi_tbl_11")
public class GovernanceConfigurationAppRole implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "bdi_attrib_30")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bdiapp_id;

	@Column(name = "bdi_attrib_31")
	private String app_nm;

	@Column(name = "bdi_attrib_32")
	private String menu_group;

	@Column(name = "bdi_attrib_33")
	private String menu_nm;

	@Column(name = "bdi_attrib_120", columnDefinition = "TINYINT")
	private Integer bdi_product_type;

	@Column(name = "bdi_attrib_34", columnDefinition = "TINYINT")
	private Integer approval_reqd_flg;

	@Column(name = "bdi_attrib_10", columnDefinition = "SMALLINT")
	@Type(type = "org.hibernate.type.ShortType")
	private Short bdiapp_role;

	@Column(name = "bdi_attrib_35", columnDefinition = "TINYINT")
	private Integer user_defined_flg;

	@Column(name = "bdi_attrib_36")
	private String menu_action_url;

	@Column(name = "bdi_attrib_37", columnDefinition = "TINYINT")
	private Integer menu_seq;

	public Integer getBdiapp_id() {
		return bdiapp_id;
	}

	public void setBdiapp_id(Integer bdiapp_id) {
		this.bdiapp_id = bdiapp_id;
	}

	public String getApp_nm() {
		return app_nm;
	}

	public void setApp_nm(String app_nm) {
		this.app_nm = app_nm;
	}

	public String getMenu_group() {
		return menu_group;
	}

	public void setMenu_group(String menu_group) {
		this.menu_group = menu_group;
	}

	public String getMenu_nm() {
		return menu_nm;
	}

	public void setMenu_nm(String menu_nm) {
		this.menu_nm = menu_nm;
	}

	public Integer getBdi_product_type() {
		return bdi_product_type;
	}

	public void setBdi_product_type(Integer bdi_product_type) {
		this.bdi_product_type = bdi_product_type;
	}

	public Integer getApproval_reqd_flg() {
		return approval_reqd_flg;
	}

	public void setApproval_reqd_flg(Integer approval_reqd_flg) {
		this.approval_reqd_flg = approval_reqd_flg;
	}

	public Short getBdiapp_role() {
		return bdiapp_role;
	}

	public void setBdiapp_role(Short bdiapp_role) {
		this.bdiapp_role = bdiapp_role;
	}

	public Integer getUser_defined_flg() {
		return user_defined_flg;
	}

	public void setUser_defined_flg(Integer user_defined_flg) {
		this.user_defined_flg = user_defined_flg;
	}

	public String getMenu_action_url() {
		return menu_action_url;
	}

	public void setMenu_action_url(String menu_action_url) {
		this.menu_action_url = menu_action_url;
	}

	public Integer getMenu_seq() {
		return menu_seq;
	}

	public void setMenu_seq(Integer menu_seq) {
		this.menu_seq = menu_seq;
	}

}
