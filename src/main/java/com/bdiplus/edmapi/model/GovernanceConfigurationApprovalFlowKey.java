package com.bdiplus.edmapi.model;

import java.io.Serializable;

public class GovernanceConfigurationApprovalFlowKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int govApplId;
	private Byte approval_level;

	public int getGovApplId() {
		return govApplId;
	}

	public void setGovApplId(int govApplId) {
		this.govApplId = govApplId;
	}

	public Byte getApproval_level() {
		return approval_level;
	}

	public void setApproval_level(Byte approval_level) {
		this.approval_level = approval_level;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((approval_level == null) ? 0 : approval_level.hashCode());
		result = prime * result + govApplId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GovernanceConfigurationApprovalFlowKey other = (GovernanceConfigurationApprovalFlowKey) obj;
		if (approval_level == null) {
			if (other.approval_level != null)
				return false;
		} else if (!approval_level.equals(other.approval_level))
			return false;
		if (govApplId != other.govApplId)
			return false;
		return true;
	}

}
