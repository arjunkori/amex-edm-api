package com.bdiplus.edmapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name ="bdi_tbl_10")
public class GovernanceConfigurationRole {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@javax.persistence.Column(name = "bdi_attrib_10",columnDefinition = "SMALLINT")
	@Type(type = "org.hibernate.type.ShortType")
	private Short bdiappRole;
	
	@Column(name = "bdi_attrib_11")
	private String bdiapp_role_nm;
	
	@Column(name = "bdi_attrib_120", columnDefinition = "TINYINT")
	private Integer cluster_optimization_type;
	
	@Column(name = "bdi_attrib_12")
	private String first_approver_nm;
	
	@Column(name = "bdi_attrib_13")
	private String first_approver_email;
	
	@Column(name = "bdi_attrib_14")
	private String Second_approver_nm;
	
	@Column(name = "bdi_attrib_15")
	private String Second_approver_email;
	
	@Column(name = "bdi_attrib_16")
	private String bdiapp_role_cat;

	public Short getBdiappRole() {
		return bdiappRole;
	}

	public void setBdiappRole(Short bdiappRole) {
		this.bdiappRole = bdiappRole;
	}

	public String getBdiapp_role_nm() {
		return bdiapp_role_nm;
	}

	public void setBdiapp_role_nm(String bdiapp_role_nm) {
		this.bdiapp_role_nm = bdiapp_role_nm;
	}

	public Integer getCluster_optimization_type() {
		return cluster_optimization_type;
	}

	public void setCluster_optimization_type(Integer cluster_optimization_type) {
		this.cluster_optimization_type = cluster_optimization_type;
	}

	public String getFirst_approver_nm() {
		return first_approver_nm;
	}

	public void setFirst_approver_nm(String first_approver_nm) {
		this.first_approver_nm = first_approver_nm;
	}

	public String getFirst_approver_email() {
		return first_approver_email;
	}

	public void setFirst_approver_email(String first_approver_email) {
		this.first_approver_email = first_approver_email;
	}

	public String getSecond_approver_nm() {
		return Second_approver_nm;
	}

	public void setSecond_approver_nm(String second_approver_nm) {
		Second_approver_nm = second_approver_nm;
	}

	public String getSecond_approver_email() {
		return Second_approver_email;
	}

	public void setSecond_approver_email(String second_approver_email) {
		Second_approver_email = second_approver_email;
	}
	
	public String getBdiapp_role_cat() {
		return bdiapp_role_cat;
	}

	public void setBdiapp_role_cat(String bdiapp_role_cat) {
		this.bdiapp_role_cat = bdiapp_role_cat;
	}
}
	
	
	