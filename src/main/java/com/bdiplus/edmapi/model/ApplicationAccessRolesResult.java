package com.bdiplus.edmapi.model;

import javax.persistence.Id;

public class ApplicationAccessRolesResult {
	
	@Id
	private int bdiapp_role;
	
	private String bdiapp_role_nm;

	private String first_approver_nm;
	
	private String first_approver_email;
	
	private String second_approver_nm;
	
	private String second_approver_email;
	
	private String bdiapp_role_cat;
	
	private String bdiapp_id;
	
	private String app_nm;

	

	public int getBdiapp_role() {
		return bdiapp_role;
	}

	public void setBdiapp_role(int bdiapp_role) {
		this.bdiapp_role = bdiapp_role;
	}

	public String getBdiapp_role_nm() {
		return bdiapp_role_nm;
	}

	public void setBdiapp_role_nm(String bdiapp_role_nm) {
		this.bdiapp_role_nm = bdiapp_role_nm;
	}

	public String getFirst_approver_nm() {
		return first_approver_nm;
	}

	public void setFirst_approver_nm(String first_approver_nm) {
		this.first_approver_nm = first_approver_nm;
	}

	public String getFirst_approver_email() {
		return first_approver_email;
	}

	public void setFirst_approver_email(String first_approver_email) {
		this.first_approver_email = first_approver_email;
	}

	public String getSecond_approver_nm() {
		return second_approver_nm;
	}

	public void setSecond_approver_nm(String second_approver_nm) {
		this.second_approver_nm = second_approver_nm;
	}

	public String getSecond_approver_email() {
		return second_approver_email;
	}

	public void setSecond_approver_email(String second_approver_email) {
		this.second_approver_email = second_approver_email;
	}

	public String getBdiapp_role_cat() {
		return bdiapp_role_cat;
	}

	public void setBdiapp_role_cat(String bdiapp_role_cat) {
		this.bdiapp_role_cat = bdiapp_role_cat;
	}

	public String getBdiapp_id() {
		return bdiapp_id;
	}

	public void setBdiapp_id(String bdiapp_id) {
		this.bdiapp_id = bdiapp_id;
	}

	public String getApp_nm() {
		return app_nm;
	}

	public void setApp_nm(String app_nm) {
		this.app_nm = app_nm;
	}
	
	
}
