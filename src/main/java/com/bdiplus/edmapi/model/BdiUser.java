package com.bdiplus.edmapi.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "bdi_tbl_13")
public class BdiUser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bdi_attrib_47")
	@JsonProperty("user_id")
	private Integer userId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bdi_attrib_48", columnDefinition="DATE")
	private Date creation_date;

	@Column(name = "bdi_attrib_49")
	private Integer user_type;

	@Column(name = "bdi_attrib_50")
	private String user_title;

	@Column(name = "bdi_attrib_51")
	private String first_name;

	@Column(name = "bdi_attrib_52")
	private String middle_name;

	@Column(name = "bdi_attrib_53")
	private String last_name;

	@Column(name = "bdi_attrib_54")
	private String department_name;

	@Column(name = "bdi_attrib_55")
	private String user_phone;

	@Column(name = "bdi_attrib_56")
	private String user_ph_for_txt;

	@Column(name = "bdi_attrib_57")
	private String user_email;

	@Column(name = "bdi_attrib_58")
	private Integer user_status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bdi_attrib_59")
	private Date status_chng_date;

	@Column(name = "bdi_attrib_60")
	private Integer status_chng_by;

	@Column(name = "bdi_attrib_61")
	private String portalLogin;

	@Column(name = "bdi_attrib_62")
	private String portal_pwd;

	@Column(name = "bdi_attrib_63")
	private String employeeId;

	@Column(name = "bdi_attrib_64")
	private String mgr_full_name;

	@Column(name = "bdi_attrib_65")
	private String mgr_email;

	@Column(name = "bdi_attrib_66")
	private Integer employee_type;

	@Lob
	@Column(name = "bdi_attrib_67", columnDefinition = "blob")
	private byte[] user_image;

	@Column(name = "bdi_attrib_68")
	private String home_page;
	
	@Column(name = "bdi_attrib_260")
	private Integer buId;
	
	@Column(name = "bdi_attrib_291")
	private Integer bu_funcId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}

	public Integer getUser_type() {
		return user_type;
	}

	public void setUser_type(Integer user_type) {
		this.user_type = user_type;
	}

	public String getUser_title() {
		return user_title;
	}

	public void setUser_title(String user_title) {
		this.user_title = user_title;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getDepartment_name() {
		return department_name;
	}

	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}

	public String getUser_phone() {
		return user_phone;
	}

	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}

	public String getUser_ph_for_txt() {
		return user_ph_for_txt;
	}

	public void setUser_ph_for_txt(String user_ph_for_txt) {
		this.user_ph_for_txt = user_ph_for_txt;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public Integer getUser_status() {
		return user_status;
	}

	public void setUser_status(Integer user_status) {
		this.user_status = user_status;
	}

	public Date getStatus_chng_date() {
		return status_chng_date;
	}

	public void setStatus_chng_date(Date status_chng_date) {
		this.status_chng_date = status_chng_date;
	}

	public Integer getStatus_chng_by() {
		return status_chng_by;
	}

	public void setStatus_chng_by(Integer status_chng_by) {
		this.status_chng_by = status_chng_by;
	}

	public String getPortalLogin() {
		return portalLogin;
	}

	public void setPortalLogin(String portalLogin) {
		this.portalLogin = portalLogin;
	}

	public String getPortal_pwd() {
		return portal_pwd;
	}

	public void setPortal_pwd(String portal_pwd) {
		this.portal_pwd = portal_pwd;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getMgr_full_name() {
		return mgr_full_name;
	}

	public void setMgr_full_name(String mgr_full_name) {
		this.mgr_full_name = mgr_full_name;
	}

	public String getMgr_email() {
		return mgr_email;
	}

	public void setMgr_email(String mgr_email) {
		this.mgr_email = mgr_email;
	}

	public Integer getEmployee_type() {
		return employee_type;
	}

	public void setEmployee_type(Integer employee_type) {
		this.employee_type = employee_type;
	}

	public byte[] getUser_image() {
		return user_image;
	}

	public void setUser_image(byte[] user_image) {
		this.user_image = user_image;
	}

	public String getHome_page() {
		return home_page;
	}

	public void setHome_page(String home_page) {
		this.home_page = home_page;
	}

	public Integer getBuId() {
		return buId;
	}

	public void setBuId(Integer buId) {
		this.buId = buId;
	}

	public Integer getBu_funcId() {
		return bu_funcId;
	}

	public void setBu_funcId(Integer bu_funcId) {
		this.bu_funcId = bu_funcId;
	}	
}



