package com.bdiplus.edmapi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


@Entity
@Table(name ="bdi_tbl_12")
@IdClass(BdiAppsRolesKey.class)
public class BdiAppsRoles implements Serializable {

	@Id
	@Column(name = "bdi_attrib_30")
	private int bdiapp_id;
	
	@Id
	@Column(name = "bdi_attrib_10")
	private short bdiappRole;


	public int getBdiapp_id() {
		return bdiapp_id;
	}


	public void setBdiapp_id(int bdiapp_id) {
		this.bdiapp_id = bdiapp_id;
	}


	public short getBdiappRole() {
		return bdiappRole;
	}


	public void setBdiappRole(short bdiappRole) {
		this.bdiappRole = bdiappRole;
	}

	
	
}
