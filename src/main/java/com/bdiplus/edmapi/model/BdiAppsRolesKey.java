package com.bdiplus.edmapi.model;

import java.io.Serializable;

public class BdiAppsRolesKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int bdiapp_id;
	private short bdiappRole;

	public int getBdiapp_id() {
		return bdiapp_id;
	}

	public void setBdiapp_id(int bdiapp_id) {
		this.bdiapp_id = bdiapp_id;
	}

	public short getBdiappRole() {
		return bdiappRole;
	}

	public void setBdiappRole(short bdiappRole) {
		this.bdiappRole = bdiappRole;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bdiappRole;
		result = prime * result + bdiapp_id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BdiAppsRolesKey other = (BdiAppsRolesKey) obj;
		if (bdiappRole != other.bdiappRole)
			return false;
		if (bdiapp_id != other.bdiapp_id)
			return false;
		return true;
	}
	
	

	
}
