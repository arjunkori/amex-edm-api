package com.bdiplus.edmapi.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "bdi_tbl_420")
public class BDIReferenceData {
	
    public BDIReferenceData () {}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bdi_attrib_1802")
	@JsonProperty("display_order")
	private Integer displayOrder;

	@Column(name = "bdi_attrib_1800")
	@JsonProperty("table_nm")
	private String tableName;

	@Column(name = "bdi_attrib_1801")
	@JsonProperty("column_nm")
	private String columnName;

	@Column(name = "bdi_attrib_1803")
	@JsonProperty("ref_value")
	private String refValue;

	@Column(name = "bdi_attrib_1804")
	private String ref_key;

	@Column(name = "bdi_attrib_1805")
	private String is_default;

	@Column(name = "bdi_attrib_1806")
	private String is_active;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getRef_key() {
		return ref_key;
	}

	public void setRef_key(String ref_key) {
		this.ref_key = ref_key;
	}

	public String getIs_default() {
		return is_default;
	}

	public void setIs_default(String is_default) {
		this.is_default = is_default;
	}

	public String getIs_active() {
		return is_active;
	}

	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}

}
