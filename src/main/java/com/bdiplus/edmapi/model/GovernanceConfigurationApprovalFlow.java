package com.bdiplus.edmapi.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="bdi_tbl_27")
@IdClass(GovernanceConfigurationApprovalFlowKey.class)
public class GovernanceConfigurationApprovalFlow implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "bdi_attrib_400")
	private int govApplId;
	
	@Column(name = "bdi_attrib_100", columnDefinition = "TINYINT")
	private Byte bdi_product_type;
	
	@Column(name = "bdi_attrib_401", columnDefinition = "TINYINT")
	private Byte approvalType;
	
	@Column(name = "bdi_attrib_402", columnDefinition = "TINYINT")
	private Byte with_sensitive_flg;
	
	@Column(name="bdi_attrib_311",columnDefinition = "SMALLINT")
	@Type(type = "org.hibernate.type.ShortType")
	private Short data_sensitive_type;
	
 	
 	@Column(name = "bdi_attrib_260")
	private Integer bu_id;
	
	@Column(name = "bdi_attrib_291")
	private Integer bu_func_id;
	
	@Column(name = "bdi_attrib_445", columnDefinition = "TINYINT")
	private Byte use_case_type;
	
	
	@Column(name="bdi_attrib_342",columnDefinition = "SMALLINT")
	@Type(type = "org.hibernate.type.ShortType")
	private Short access_role_id;
	
	@Id
	@Column(name = "bdi_attrib_403", columnDefinition = "TINYINT")
	private Byte approval_level;
	
	@Column(name = "bdi_attrib_404", columnDefinition = "TINYINT")
	private Byte data_usage_types;
	
	//newly added
	@Column(name = "bdi_attrib_412")
	private Long approver_type;
	
	
	@Column(name = "bdi_attrib_405")
	private String approver1_nm;
	
	@Column(name = "bdi_attrib_406")
	private String approver1_title;
	
	@Column(name = "bdi_attrib_407")
	private String approver1_email;
	
	@Column(name = "bdi_attrib_408")
	private String approver2_nm;
	
	@Column(name = "bdi_attrib_409")
	private String approver2_title;
	
	@Column(name = "bdi_attrib_410")
	private String approver2_email;
	
	@Column(name = "bdi_attrib_800")
	private Long platf_id;
	
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "bdi_attrib_411", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updt_dt;
	
	
	@Column(name = "bdi_attrib_47")
	private int updt_by_id;
	
	@Column(name = "bdi_attrib_413")
	private String owner_type_name;
	
	
	public int getGovApplId() {
		return govApplId;
	}


	public void setGovApplId(int govApplId) {
		this.govApplId = govApplId;
	}


	public Byte getBdi_product_type() {
		return bdi_product_type;
	}


	public void setBdi_product_type(Byte bdi_product_type) {
		this.bdi_product_type = bdi_product_type;
	}

	public Byte getApprovalType() {
		return approvalType;
	}


	public void setApprovalType(Byte approvalType) {
		this.approvalType = approvalType;
	}


	public Byte getWith_sensitive_flg() {
		return with_sensitive_flg;
	}


	public void setWith_sensitive_flg(Byte with_sensitive_flg) {
		this.with_sensitive_flg = with_sensitive_flg;
	}


	public Short getData_sensitive_type() {
		return data_sensitive_type;
	}


	public void setData_sensitive_type(Short data_sensitive_type) {
		this.data_sensitive_type = data_sensitive_type;
	}


	public Integer getBu_id() {
		return bu_id;
	}


	public void setBu_id(Integer bu_id) {
		this.bu_id = bu_id;
	}


	public Integer getBu_func_id() {
		return bu_func_id;
	}


	public void setBu_func_id(Integer bu_func_id) {
		this.bu_func_id = bu_func_id;
	}


	public Byte getUse_case_type() {
		return use_case_type;
	}


	public void setUse_case_type(Byte use_case_type) {
		this.use_case_type = use_case_type;
	}


	public Short getAccess_role_id() {
		return access_role_id;
	}


	public void setAccess_role_id(Short access_role_id) {
		this.access_role_id = access_role_id;
	}


	public Byte getApproval_level() {
		return approval_level;
	}


	public void setApproval_level(Byte approval_level) {
		this.approval_level = approval_level;
	}


	public Byte getData_usage_types() {
		return data_usage_types;
	}


	public void setData_usage_types(Byte data_usage_types) {
		this.data_usage_types = data_usage_types;
	}


	public String getApprover1_nm() {
		return approver1_nm;
	}


	public void setApprover1_nm(String approver1_nm) {
		this.approver1_nm = approver1_nm;
	}


	public String getApprover1_title() {
		return approver1_title;
	}


	public void setApprover1_title(String approver1_title) {
		this.approver1_title = approver1_title;
	}


	public String getApprover1_email() {
		return approver1_email;
	}


	public void setApprover1_email(String approver1_email) {
		this.approver1_email = approver1_email;
	}


	public String getApprover2_nm() {
		return approver2_nm;
	}


	public void setApprover2_nm(String approver2_nm) {
		this.approver2_nm = approver2_nm;
	}


	public String getApprover2_title() {
		return approver2_title;
	}


	public void setApprover2_title(String approver2_title) {
		this.approver2_title = approver2_title;
	}


	public String getApprover2_email() {
		return approver2_email;
	}


	public void setApprover2_email(String approver2_email) {
		this.approver2_email = approver2_email;
	}


	public Date getUpdt_dt() {
		return updt_dt;
	}


	public void setUpdt_dt(Date updt_dt) {
		this.updt_dt = updt_dt;
	}


	public int getUpdt_by_id() {
		return updt_by_id;
	}


	public void setUpdt_by_id(int updt_by_id) {
		this.updt_by_id = updt_by_id;
	}


	public Long getApprover_type() {
		return approver_type;
	}


	public void setApprover_type(Long approver_type) {
		this.approver_type = approver_type;
	}


	public Long getPlatf_id() {
		return platf_id;
	}


	public void setPlatf_id(Long platf_id) {
		this.platf_id = platf_id;
	}


	public String getOwner_type_name() {
		return owner_type_name;
	}


	public void setOwner_type_name(String owner_type_name) {
		this.owner_type_name = owner_type_name;
	}
	
	
}
