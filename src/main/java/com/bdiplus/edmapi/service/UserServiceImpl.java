package com.bdiplus.edmapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdiplus.edmapi.model.BdiUser;
import com.bdiplus.edmapi.repository.BdiUserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	private BdiUserRepository bdiUserRepository;
	
	@Autowired
	public UserServiceImpl(BdiUserRepository bdiUserRepository) {
		this.bdiUserRepository = bdiUserRepository;
	}

	@Override
	public List<BdiUser> getAllUsers() {
		return bdiUserRepository.findAll();		
	}

}
