package com.bdiplus.edmapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdiplus.edmapi.model.BDIReferenceData;
import com.bdiplus.edmapi.repository.BDIReferenceDataRepository;

@Service
public class BDIReferenceDataServiceImpl implements BDIReferenceDataService{
	
	private BDIReferenceDataRepository bDIReferenceDataRepository;
	
	@Autowired
	public BDIReferenceDataServiceImpl(BDIReferenceDataRepository bDIReferenceDataRepository) {
		this.bDIReferenceDataRepository = bDIReferenceDataRepository;
	}
	
	public BDIReferenceData saveBDIReferenceData(BDIReferenceData bdiReferenceData) {
		return bDIReferenceDataRepository.save(bdiReferenceData);
	}

	public List getBDIReferenceDataID(String tableName, String columnName) {
		return bDIReferenceDataRepository.findByTableNameAndColumnName(tableName, columnName);
	}

	public void deleteBDIReferenceData(BDIReferenceData bdiReferenceData) {
		bDIReferenceDataRepository.delete(bdiReferenceData); 
	}

	public BDIReferenceData updateBDIReferenceData(BDIReferenceData bdiReferenceData) {
		return bDIReferenceDataRepository.save(bdiReferenceData);
	}

	public List getAllBDIReferenceData() {
		return bDIReferenceDataRepository.findAll();
	}
	
	public List<BDIReferenceData> getBDIReferenceData(String tableName, String columnName, String refValue) {
		return bDIReferenceDataRepository.findByTableNameAndColumnNameAndRefValue(tableName, columnName, refValue);
	}

	@Override
	public List<BDIReferenceData> getStorageFormat() {
		// TODO Auto-generated method stub
		//return null;
		return bDIReferenceDataRepository.getStorageFormat();
	}

	@Override
	public List<BDIReferenceData> getDataFormat() {
		// TODO Auto-generated method stub
		//return null;
		return bDIReferenceDataRepository.getDataFormat();
	}

	@Override
	public List<BDIReferenceData> getDataIngestionTimeFrameunit() {
		// TODO Auto-generated method stub
		return bDIReferenceDataRepository.getDataIngestionTimeFrameunit();
	}

	@Override
	public List<BDIReferenceData> getBDIReferenceData(String tableName, String columnName, Integer refValue) {
		return bDIReferenceDataRepository.getBDIReferenceData(tableName, columnName, refValue);
	}
	
	@Override
	public List<BDIReferenceData> getLoadTypeData() {
		// TODO Auto-generated method stub
		//return null;
		return bDIReferenceDataRepository.getLoadTypeData();
	}


}
