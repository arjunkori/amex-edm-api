package com.bdiplus.edmapi.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bdiplus.edmapi.dto.StatusMessage;
import com.bdiplus.edmapi.model.ApplicationAccessRolesResult;
import com.bdiplus.edmapi.model.BdiAppsRoles;
import com.bdiplus.edmapi.model.GovernanceConfigurationApp;
import com.bdiplus.edmapi.model.GovernanceConfigurationAppRole;
import com.bdiplus.edmapi.model.GovernanceConfigurationApprovalFlow;
import com.bdiplus.edmapi.model.GovernanceConfigurationDataAllowed;
import com.bdiplus.edmapi.model.GovernanceConfigurationRole;
import com.bdiplus.edmapi.repository.BdiAppsRolesRepository;
//import com.bdiplus.edmapi.repository.BdiPortalUserRolesRepository;
import com.bdiplus.edmapi.repository.GovernanceConfigurationAppRepository;
import com.bdiplus.edmapi.repository.GovernanceConfigurationAppRoleRepository;
import com.bdiplus.edmapi.repository.GovernanceConfigurationApprovalFlowRepository;
import com.bdiplus.edmapi.repository.GovernanceConfigurationDataAllowedRepository;
import com.bdiplus.edmapi.repository.GovernanceConfigurationRoleRepository;

@Service
public class GovernanceServiceImpl implements GovernanceService {
	private GovernanceConfigurationDataAllowedRepository governanceConfigurationDataAllowedRepository;
	private GovernanceConfigurationAppRepository governanceConfigurationAppRepository;
	//private BdiPortalUserRolesRepository bdiPortalUserRolesRepository;
	private GovernanceConfigurationAppRoleRepository governanceConfigurationAppRoleRepository;
	private GovernanceConfigurationRoleRepository governanceConfigurationRoleRepository;
	private BdiAppsRolesRepository bdiAppsRolesRepository;
	private GovernanceConfigurationApprovalFlowRepository governanceConfigurationApprovalFlowRepository;
	@Autowired
	public GovernanceServiceImpl(GovernanceConfigurationAppRepository governanceConfigurationAppRepository,	GovernanceConfigurationDataAllowedRepository governanceConfigurationDataAllowedRepository,
			//BdiPortalUserRolesRepository bdiPortalUserRolesRepository, 
			GovernanceConfigurationApprovalFlowRepository governanceConfigurationApprovalFlowRepository,
			GovernanceConfigurationRoleRepository governanceConfigurationRoleRepository,
			BdiAppsRolesRepository bdiAppsRolesRepository,GovernanceConfigurationAppRoleRepository governanceConfigurationAppRoleRepository
) {
		this.governanceConfigurationDataAllowedRepository = governanceConfigurationDataAllowedRepository;
		this.governanceConfigurationAppRepository = governanceConfigurationAppRepository;
		//this.bdiPortalUserRolesRepository = bdiPortalUserRolesRepository;
		this.governanceConfigurationRoleRepository = governanceConfigurationRoleRepository;
		this.bdiAppsRolesRepository = bdiAppsRolesRepository;
		this.governanceConfigurationApprovalFlowRepository = governanceConfigurationApprovalFlowRepository;
		this.governanceConfigurationAppRoleRepository = governanceConfigurationAppRoleRepository;
	}
	@SuppressWarnings("rawtypes")
	private StatusMessage returnMessage(String message, int code) {
		StatusMessage messageStatus = new StatusMessage();
		messageStatus.setStatusCode(code);
		messageStatus.setMessage(message);
		return messageStatus;
	}
	
	// Data Allowed
		public List getDataAllowed() {
			return governanceConfigurationDataAllowedRepository.findAll();
		}

		public StatusMessage createDataAllowed(GovernanceConfigurationDataAllowed dataAllow, HttpServletRequest req) {

			System.out.println("******Entered Service****");
			StatusMessage msg = new StatusMessage();
			try {
				governanceConfigurationDataAllowedRepository.save(dataAllow);
			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot create user !");
				msg.setStatusCode(403);
			}

			return returnMessage("User created successfully", 200);
		}

		public StatusMessage deleteSensitiveData(GovernanceConfigurationDataAllowed sensitive_data,
				HttpServletRequest req) {
			StatusMessage msg = new StatusMessage();
			try {
				governanceConfigurationDataAllowedRepository.delete(sensitive_data);
			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot delete sensitive data!!");
				msg.setStatusCode(403);
			}

			return returnMessage("Sensitive data deleted successfully", 200);
		}
		public GovernanceConfigurationDataAllowed findByDataAttributeId(short id) {
			return governanceConfigurationDataAllowedRepository.findByDataAttributeId(id);
		}
		
		
		// application approval
		@Transactional
		public StatusMessage updateApprovalFlg(GovernanceConfigurationApp gov, HttpServletRequest req) {
			StatusMessage msg = new StatusMessage();
			try {
				governanceConfigurationAppRepository.updateApprovalFlg(gov.getApproval_reqd_flg(), gov.getBdiapp_id());
			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot update!");
				msg.setStatusCode(403);
			}
			return returnMessage("Updated successfully", 200);
		}
		
		public StatusMessage deleteApplicationApproval(GovernanceConfigurationApp appln_approval, HttpServletRequest req) {
			StatusMessage msg = new StatusMessage();
			try {
				governanceConfigurationAppRepository.delete(appln_approval);
			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot delete application approval!!");
				msg.setStatusCode(403);
			}

			return returnMessage("Application approval deleted successfully", 200);
		}

		//public	GovernanceConfigurationApp findByDataAppId(int id){
		//	return governanceConfigurationAppRepository.findByDataAppId(id);
		//}
		
		
		
		@Override
		public List userRoles() {
			return governanceConfigurationRoleRepository.userRoles();
		}

		
		
		// roles section
		public int saveRole(GovernanceConfigurationRole role, HttpServletRequest req) {
			int identifier = 0;
			StatusMessage msg = new StatusMessage();
			try {
				identifier = governanceConfigurationRoleRepository.save(role).getBdiappRole();

			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot create user !");
				msg.setStatusCode(403);
			}

			return identifier;
		}

		public StatusMessage saveAppsRole(BdiAppsRoles app_role, HttpServletRequest req) {

			StatusMessage msg = new StatusMessage();
			try {
				bdiAppsRolesRepository.save(app_role);
			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot create user !");
				msg.setStatusCode(403);
			}

			return returnMessage("Role created successfully", 200);
		}

		public void saveApp(GovernanceConfigurationApp app, HttpServletRequest req) {
			governanceConfigurationAppRepository.save(app);
		}

		public void saveAppRole(GovernanceConfigurationAppRole app, HttpServletRequest req) {
			governanceConfigurationAppRoleRepository.save(app);
		}

		@Transactional
		public StatusMessage deleteApplicationAccess(GovernanceConfigurationRole role, HttpServletRequest req) {
			StatusMessage msg = new StatusMessage();
			try {
				bdiAppsRolesRepository.deleteByBdiappRole(role.getBdiappRole());
				governanceConfigurationRoleRepository.delete(role);
			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot delete application access!!");
				msg.setStatusCode(403);
			}

			return returnMessage("Application access deleted successfully", 200);
		}

		@Transactional
		public StatusMessage deleteAppRoles(BdiAppsRoles appRoleDelete, HttpServletRequest req) {
			StatusMessage msg = new StatusMessage();
			try {
				bdiAppsRolesRepository.deleteByBdiappRole(appRoleDelete.getBdiappRole());
				msg = returnMessage("AppsRoles deleted successfully", 200);
			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot delete appsRoles record!!");
				msg.setStatusCode(403);
			}
			return msg;
		}
		@Override
		public List getAllData() {
		//	return governanceConfigurationRoleRepository.getAllData();
			//------------------------
			List input = governanceConfigurationRoleRepository.getAllData();;
			List<ApplicationAccessRolesResult> finalList = new ArrayList<ApplicationAccessRolesResult>();
			Iterator itr = input.iterator();
			while (itr.hasNext()) {
				Object[] objArray = (Object[]) itr.next();
				ApplicationAccessRolesResult obj = new ApplicationAccessRolesResult();

				obj.setBdiapp_role((short)objArray[0]);
				obj.setBdiapp_role_nm((String) objArray[1]);
				obj.setFirst_approver_nm((String) objArray[2]);
				obj.setFirst_approver_email((String) objArray[3]);
				obj.setSecond_approver_nm((String) objArray[4]);
				obj.setSecond_approver_email((String) objArray[5]);
				//obj.setBdiapp_role_cat((String) objArray[6]);
				obj.setBdiapp_role_cat(objArray[6].toString());
				obj.setBdiapp_id((String) objArray[7]);
				obj.setApp_nm((String) objArray[8]);
				
				
				finalList.add(obj);
			}
			return finalList;

			
		}

		@Override
		public List getAllApp(int bdi_product_type) {
			return governanceConfigurationAppRepository.findAll(bdi_product_type);
		}

		// Approval flow

		public StatusMessage createApprovalFlow(GovernanceConfigurationApprovalFlow approvalFlow, HttpServletRequest req) {
			StatusMessage msg = new StatusMessage();
			try {
				governanceConfigurationApprovalFlowRepository.save(approvalFlow);
			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot create approval flow!");
				msg.setStatusCode(403);
			}

			return returnMessage("Approval flow created successfully", 200);
		}

		@Override
		public List getAllApprovalFlow(GovernanceConfigurationApprovalFlow approval_type) {
			return governanceConfigurationApprovalFlowRepository.findByApprovalType(approval_type.getApprovalType());
		}
		

		// row count
		public long getRowCount() {
			return governanceConfigurationApprovalFlowRepository.count();
		}

		public Integer getMaxId() {
			return governanceConfigurationApprovalFlowRepository.findTop1ByGovApplId();
		}

		public StatusMessage deleteApprovalFlow(GovernanceConfigurationApprovalFlow appflow, HttpServletRequest req) {
			StatusMessage msg = new StatusMessage();
			try {
			//	governanceConfigurationApprovalFlowRepository.delete(appflow);
				governanceConfigurationApprovalFlowRepository.deleteUsingGovApplId(appflow.getGovApplId());
			} catch (Exception e) {
				e.printStackTrace();
				msg.setMessage("Error:Cannot delete approvalFlow !!");
				msg.setStatusCode(403);
			}

			return returnMessage("Approval flow deleted successfully", 200);
		}
		
}

