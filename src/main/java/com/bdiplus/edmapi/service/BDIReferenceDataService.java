package com.bdiplus.edmapi.service;

import java.util.List;

import com.bdiplus.edmapi.model.BDIReferenceData;


public interface BDIReferenceDataService {

	public BDIReferenceData saveBDIReferenceData(BDIReferenceData bdiReferenceData);

	public List getBDIReferenceDataID(String tableName, String columnName);
	
	public List getAllBDIReferenceData();

	public void deleteBDIReferenceData(BDIReferenceData bdiReferenceData);

	public BDIReferenceData updateBDIReferenceData(BDIReferenceData bdiReferenceData);
	
	public List<BDIReferenceData> getBDIReferenceData(String tableName, String columnName, String refValue);
	
	List<BDIReferenceData>getStorageFormat();
	List<BDIReferenceData>getDataFormat();
	List<BDIReferenceData> getDataIngestionTimeFrameunit();
	List<BDIReferenceData> getLoadTypeData();

	public List<BDIReferenceData> getBDIReferenceData(String tableName, String columnName, Integer dataValueIndicator);
	
	
}