package com.bdiplus.edmapi.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.bdiplus.edmapi.dto.StatusMessage;
import com.bdiplus.edmapi.model.BdiAppsRoles;
import com.bdiplus.edmapi.model.GovernanceConfigurationApp;
import com.bdiplus.edmapi.model.GovernanceConfigurationAppRole;
import com.bdiplus.edmapi.model.GovernanceConfigurationApprovalFlow;
import com.bdiplus.edmapi.model.GovernanceConfigurationDataAllowed;
import com.bdiplus.edmapi.model.GovernanceConfigurationRole;




public interface GovernanceService {
	

	public List getDataAllowed();
	
	public StatusMessage createDataAllowed(GovernanceConfigurationDataAllowed dataAllow, HttpServletRequest req);
	
	public StatusMessage deleteSensitiveData(GovernanceConfigurationDataAllowed sensitive_data ,HttpServletRequest req);

	public GovernanceConfigurationDataAllowed findByDataAttributeId(short data_attr_Id);
	
	//Application approval
	
		public StatusMessage updateApprovalFlg(GovernanceConfigurationApp gov, HttpServletRequest req);

		//public StatusMessage deleteApplicationApproval(GovernanceConfigurationApp appln_approval ,HttpServletRequest req);
	
		//public GovernanceConfigurationApp findByDataAppId(int bdiapp_id);
		
		public List userRoles();
		
		
		// roles section

		public List getAllData();
		
		public List getAllApp(int bdi_product_type);

		
		public int saveRole(GovernanceConfigurationRole role,
				HttpServletRequest req);
		
		// saving bdi_apps_roles here
		
		public StatusMessage saveAppsRole(BdiAppsRoles app_role,
				HttpServletRequest req);
		

		public void saveApp(GovernanceConfigurationApp app, HttpServletRequest req);
		
		public void saveAppRole(GovernanceConfigurationAppRole app, HttpServletRequest req);
		
		public StatusMessage deleteApplicationAccess(GovernanceConfigurationRole role,HttpServletRequest req);
		
		public StatusMessage deleteAppRoles(BdiAppsRoles appRoleDelete,HttpServletRequest req);
		
		// Approval Flow
		
		public StatusMessage createApprovalFlow(GovernanceConfigurationApprovalFlow approvalFlow, HttpServletRequest req);	
		
		public List getAllApprovalFlow(GovernanceConfigurationApprovalFlow approval_type);
		
		public StatusMessage deleteApprovalFlow(GovernanceConfigurationApprovalFlow appflow,HttpServletRequest req);
		
		// row count
		
		public long getRowCount();
		
		public Integer getMaxId();
		
}