package com.bdiplus.edmapi.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.bdiplus.edmapi.service.BDIReferenceDataService;

@CrossOrigin(origins = "*")
@Transactional
@RestController
@RequestMapping("/bdi-reference-data")
public class BDIReferenceDataController {
	
	private BDIReferenceDataService bdiReferenceDataService;
	
	@Autowired
	public BDIReferenceDataController(BDIReferenceDataService bdiReferenceDataService) {
		this.bdiReferenceDataService = bdiReferenceDataService;
	}
	
	@RequestMapping(value = "/list/{table_name}/{column_name}", method = RequestMethod.GET)
	public List getUserAvailiblity(@PathVariable("table_name") String table_name, @PathVariable("column_name") String column_name, HttpServletRequest req, HttpServletResponse res) {
		List bdiReferenceDataList = new ArrayList();
		bdiReferenceDataList = bdiReferenceDataService.getBDIReferenceDataID(table_name, column_name);
		return bdiReferenceDataList;
	}
}
