/**
 * Copyright BDIplus 2019.All rights reserved. No part of this program may be photocopied
 * reproduced or translated to another program language without prior written consent of BDIplus.
 * 
 * FILE                 :  UserController.java
 * 
 * PACKAGE				:  com.bdiplus.edmapi.controller
 * 
 * AUTHOR(s)			:  ACC
 * 
 * VERSION				:  0.1
 * 
 * REVISION LOG			:
 * 
 *     Date              By		      Version			JIRA/Ticket Id			Description
 *    ------		-----------		-------------       ------------------	------------------
 *   30/01/2019         ACC             0.1                   NA            User Operations        
 */

package com.bdiplus.edmapi.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bdiplus.edmapi.dto.StatusMessage;
import com.bdiplus.edmapi.service.UserService;

@CrossOrigin("*")
@RestController
@RequestMapping("/user")
public class UserController {
	
	private static Logger logger = LoggerFactory.getLogger(UserController.class);
	
	private UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
	/**
	 * This method gets request from /list and fetches all users from table and returns users 
	 * list back to the client.
	 * @param res
	 * @param req
	 * @return users list
	 * @author acc
	 */
    @GetMapping("/list")
    public StatusMessage getUsersList(HttpServletResponse res,HttpServletRequest req) {
    	StatusMessage msg = new StatusMessage();
    	try{
    		logger.info("##### INSIDE LIST CONTROLLER ##########");
    		msg.setListData(userService.getAllUsers());
    		msg.setStatusCode(200);
    	}catch(Exception e) {
    		logger.error("##### ERROR IN USERS CONTORLLER ##########");
    		e.printStackTrace();
    		msg.setStatusCode(500);
    		res.setStatus(500);
    		msg.setMessage("unable to get user list");
    	}
		return msg;  	
    }
    
	
	
	
	

	

}
