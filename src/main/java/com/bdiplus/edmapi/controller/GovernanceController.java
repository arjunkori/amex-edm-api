package com.bdiplus.edmapi.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bdiplus.edmapi.dto.PortalUserRolesResult;
import com.bdiplus.edmapi.dto.StatusMessage;
import com.bdiplus.edmapi.dto.TableCountResult;
import com.bdiplus.edmapi.model.BDIReferenceData;
import com.bdiplus.edmapi.model.BdiAppsRoles;
import com.bdiplus.edmapi.model.GovernanceConfigurationApp;
import com.bdiplus.edmapi.model.GovernanceConfigurationApprovalFlow;
import com.bdiplus.edmapi.model.GovernanceConfigurationDataAllowed;
import com.bdiplus.edmapi.model.GovernanceConfigurationRole;
import com.bdiplus.edmapi.service.GovernanceService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/Governance")
public class GovernanceController {
	
	private static Logger logger = LoggerFactory.getLogger(GovernanceController.class);
	
	private GovernanceService ucgcService;
	@Autowired
	public GovernanceController(GovernanceService ucgcService) {
		this.ucgcService = ucgcService;
	}

	/**
	 * 
	 * @param sd
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws ParseException
	 * @throws java.text.ParseException
	 */
	
	
	// --------------------- Sensitive data classification------------------//
	@PostMapping("/sensitive_data")
	
	public StatusMessage sensitiveData(@RequestBody String sd, HttpServletRequest req, HttpServletResponse res)
			throws IOException, JSONException, ParseException, java.text.ParseException {

		StatusMessage msg = new StatusMessage();
		try {
			JSONParser parse = new JSONParser();
			org.json.simple.JSONObject jobj = (org.json.simple.JSONObject) parse.parse(sd);
			org.json.simple.JSONArray jsonarr_4 = (org.json.simple.JSONArray) jobj.get("sensitive_data");
			for (int j = 0; j < jsonarr_4.size(); j++) {
				org.json.simple.JSONObject jsonobj_4 = (org.json.simple.JSONObject) jsonarr_4.get(j);
				GovernanceConfigurationDataAllowed dataAllow = new GovernanceConfigurationDataAllowed();
				int attId = ((Long) jsonobj_4.get("data_attribute_id")).intValue();
				if (attId != 0) {
					dataAllow.setDataAttributeId(((Long) jsonobj_4.get("data_attribute_id")).shortValue());
				}
				dataAllow.setData_sensitive_type(((Long) jsonobj_4.get("data_sensitive_type")).shortValue());
				dataAllow.setData_sensitive_nm((String) jsonobj_4.get("data_sensitive_nm"));
				dataAllow.setData_attribute_nm((String) jsonobj_4.get("data_attribute_nm"));
				dataAllow.setData_attribute_desc((String) jsonobj_4.get("data_attribute_desc"));
				dataAllow.setRetention_period_unit((String) jsonobj_4.get("retention_period_unit"));
				dataAllow.setRetention_period(((Long) jsonobj_4.get("retention_period")).shortValue());
				dataAllow.setMasking_reqd_flg(((Long) jsonobj_4.get("masking_reqd_flg")).byteValue());
				dataAllow.setEncryption_reqd_flg(((Long) jsonobj_4.get("encryption_reqd_flg")).byteValue());
				dataAllow.setApproval_flg(((Long) jsonobj_4.get("approval_flg")).byteValue());

				// setting update date here
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
				String start_dt = formatter.format(new Date());

				Date date = (Date) formatter.parse(start_dt);
				dataAllow.setUpdt_dt(date);

				

				dataAllow.setUpdt_by_id(((Long) jsonobj_4.get("updt_by_id")).intValue());
				logger.info("------"+req.equals(null));
				
				ucgcService.createDataAllowed(dataAllow, req);
				if (((Long) jsonobj_4.get("data_attribute_id")).intValue() == 0) {
					msg.setMessage("Sensitive data created successfully !!");
					msg.setStatusCode(200);
				} else {
					msg.setMessage("Sensitive data updated successfully !!");
					msg.setStatusCode(200);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setStatus(406);
			msg.setMessage("sensitive data creation failed !");
		}
		return msg;
	}

	// delete for sensitive data with (data_attribute_id)

	@DeleteMapping("/delete_sensitive_data/{id}")
	
	public StatusMessage deleteSensitiveData(@PathVariable("id") short data_attr_Id, HttpServletRequest req,
			HttpServletResponse res) {
		GovernanceConfigurationDataAllowed sensitive_data = ucgcService.findByDataAttributeId(data_attr_Id);
		StatusMessage msg = new StatusMessage();
		try {
			msg = ucgcService.deleteSensitiveData(sensitive_data, req);
			if (msg.getStatusCode() == 406) {
				res.setStatus(406);
			} else {
				msg.setStatusCode(200);
				res.setStatus(200);
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setStatus(406);
			msg.setMessage("Sensitive Data deletion failed !!");
		}
		return msg;
	}

	// -------------------------------------Application Approval---------------------------------------//

	/**
	 * 
	 * @param appapprov
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws ParseException
	 * @throws java.text.ParseException
	 */
		@PostMapping("/application_approval")
		
		public StatusMessage applicationApproval(@RequestBody String appapprov, HttpServletRequest req, HttpServletResponse res)
				throws IOException, JSONException, ParseException, java.text.ParseException {

			StatusMessage msg = new StatusMessage();
			try {
				JSONParser parse = new JSONParser();
				org.json.simple.JSONObject jobj = (org.json.simple.JSONObject) parse.parse(appapprov);

				org.json.simple.JSONArray app_apprvl_array = (org.json.simple.JSONArray) jobj.get("application_approval");
				for (int j = 0; j < app_apprvl_array.size(); j++) {
					org.json.simple.JSONObject app_apprvl_obj = (org.json.simple.JSONObject) app_apprvl_array.get(j);
					GovernanceConfigurationApp govObj = new GovernanceConfigurationApp();
					try {
						govObj.setBdiapp_id(((Long) app_apprvl_obj.get("bdiapp_id")).intValue());
						govObj.setApproval_reqd_flg(((Long) app_apprvl_obj.get("approval_reqd_flg")).intValue());
						ucgcService.updateApprovalFlg(govObj, req);

						if (((Long) app_apprvl_obj.get("approval_reqd_flg")).intValue() == 0) {
							msg.setMessage("Application approval disabled!!");
							msg.setStatusCode(200);
						}
						if (((Long) app_apprvl_obj.get("approval_reqd_flg")).intValue() == 1) {
							msg.setMessage("Application approval enabled!!");
							msg.setStatusCode(200);
						}
						if ((((Long) app_apprvl_obj.get("approval_reqd_flg")).intValue() != 0 )&&(((Long) app_apprvl_obj.get("approval_reqd_flg")).intValue() != 1)){
							msg.setMessage("Application requried falg is wrong!!");
							msg.setStatusCode(200);
						}
					} catch (Exception e) {
						e.printStackTrace();
						res.setStatus(403);
				
						logger.error("Process failed");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				res.setStatus(406);
				msg.setMessage("Application approval updated failed !");
			}
			return msg;
		}

		// Delete application Approval

		/*@DeleteMapping("/delete_application_approval/{id}")
		
		public StatusMessage deleteApplicationApproval(@PathVariable("id") int bdiapp_id, HttpServletRequest req,
				HttpServletResponse res) {
			GovernanceConfigurationApp gap = ucgcService.findByDataAppId(bdiapp_id);
			StatusMessage msg = new StatusMessage();
			try {
				msg = ucgcService.deleteApplicationApproval(gap, req);
				if (msg.getStatusCode() == 406) {
					res.setStatus(406);
				} else {
					res.setStatus(200);
					msg.setStatusCode(200);
				}
			} catch (Exception e) {
				e.printStackTrace();
				res.setStatus(406);
				msg.setMessage("Application approval deletion failed !!");
			}
			return msg;
		}
		*/

		
		// ------------------get all user roles---------------------
		/**
		 * 
		 * @param req
		 * @param res
		 * @return
		 */

		
		// --------------------------------------Application Access-----------------------------------------------------------
/**
 * 
 * @param appaccess
 * @param req
 * @param res
 * @return
 * @throws IOException
 * @throws JSONException
 * @throws ParseException
 * @throws java.text.ParseException
 */
		@PostMapping(value = "/application_access")
		public StatusMessage applicationAccess(@RequestBody String appaccess, HttpServletRequest req, HttpServletResponse res)
				throws IOException, JSONException, ParseException, java.text.ParseException {

			StatusMessage msg = new StatusMessage();
			try {
				JSONParser parse = new JSONParser();
				org.json.simple.JSONObject jobj = (org.json.simple.JSONObject) parse.parse(appaccess);
				org.json.simple.JSONArray jsonarr_3 = (org.json.simple.JSONArray) jobj.get("application_access");
				// saving by iterating each record in loop
				for (int j = 0; j < jsonarr_3.size(); j++) {
					int flag = 1;
					org.json.simple.JSONObject jsonobj_3 = (org.json.simple.JSONObject) jsonarr_3.get(j);
					GovernanceConfigurationRole role = new GovernanceConfigurationRole();
					int roleId = ((Long) jsonobj_3.get("bdiapp_role")).intValue();
					role.setBdiapp_role_cat((String) jsonobj_3.get("bdiapp_role_cat"));
					role.setBdiapp_role_nm((String) jsonobj_3.get("bdiapp_role_nm"));
					role.setFirst_approver_nm((String) jsonobj_3.get("1st_approver_nm"));
					role.setFirst_approver_email((String) jsonobj_3.get("1st_approver_email"));
					role.setSecond_approver_nm((String) jsonobj_3.get("2nd_approver_nm"));
					role.setSecond_approver_email((String) jsonobj_3.get("2nd_approver_email"));
					if (roleId != 0) {
						role.setBdiappRole(((Long) jsonobj_3.get("bdiapp_role")).shortValue());
						// here call an update function
						int identifier = ucgcService.saveRole(role, req); // identifier will be zero which is okay
						// delete all the record from join table
						// for deleting records in join table, using the same delete function
						BdiAppsRoles appRoleDelete = new BdiAppsRoles();
						appRoleDelete.setBdiappRole(((Long) jsonobj_3.get("bdiapp_role")).shortValue()); // taking the same
																											// id to delete
						msg = ucgcService.deleteAppRoles(appRoleDelete, req);
						// insert those record in join table
						org.json.simple.JSONArray jsonarr_4 = (org.json.simple.JSONArray) jsonobj_3.get("bdiapp_id");
						for (int x = 0; x < jsonarr_4.size(); x++) {
							System.out.println(jsonarr_4.get(x));
							BdiAppsRoles app_role = new BdiAppsRoles();
							app_role.setBdiapp_id(((Long) jsonarr_4.get(x)).intValue());
							app_role.setBdiappRole(((Long) jsonobj_3.get("bdiapp_role")).shortValue());
							ucgcService.saveAppsRole(app_role, req);
						}
					} else {
						int identifier = ucgcService.saveRole(role, req);
						org.json.simple.JSONArray jsonarr_4 = (org.json.simple.JSONArray) jsonobj_3.get("bdiapp_id");
						for (int x = 0; x < jsonarr_4.size(); x++) {
							System.out.println(jsonarr_4.get(x));
							BdiAppsRoles app_role = new BdiAppsRoles();
							app_role.setBdiapp_id(((Long) jsonarr_4.get(x)).intValue());
							app_role.setBdiappRole((short) identifier);
							ucgcService.saveAppsRole(app_role, req);
						}
					}
					if (roleId == 0) {
						msg.setMessage("Appication access created successfully !!");
						msg.setStatusCode(200);
					} else {
						msg.setMessage("Appication access updated successfully !!");
						msg.setStatusCode(200);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				res.setStatus(406);
				msg.setMessage("Appication access creation failed !");
			}
			return msg;
		}

		// Delete Application Access

		@DeleteMapping(value = "/delete_application_access/{id}")
		
		public StatusMessage deleteApplicationAccess(@PathVariable("id") short bdiapp_role, HttpServletRequest req,
				HttpServletResponse res) {

			GovernanceConfigurationRole role = new GovernanceConfigurationRole();
			role.setBdiappRole(bdiapp_role);
			StatusMessage msg = new StatusMessage();
			try {
				msg = ucgcService.deleteApplicationAccess(role, req);
				if (msg.getStatusCode() == 406) {
					res.setStatus(406);
				} else {
					res.setStatus(200);
					msg.setStatusCode(200);
				}
			} catch (Exception e) {
				e.printStackTrace();
				res.setStatus(406);
				msg.setMessage("Application access deletion failed !!");
			}
			return msg;
		}
//-------------------------------------roles list--------------
		/*@GetMapping(value = "/list_user_roles")
		
		public List getUserRoles(HttpServletRequest req, HttpServletResponse res) {
			List roleList = new ArrayList();
			List finalList = new ArrayList();
			try {
				roleList = ucgcService.userRoles();
				for (int i = 0; i < roleList.size(); i++) {
					Object[] result = (Object[]) roleList.get(i);
					GovernanceConfigurationRole role = new GovernanceConfigurationRole();
					role.setBdiappRole((short) result[0]);
					role.setBdiapp_role_nm((String) result[1]);

					finalList.add(role);
				}

			} catch (Exception e) {
				e.printStackTrace();
				res.setStatus(403);
			}
			res.setStatus(200);
			return finalList;

		}
		*/
		
		

		@RequestMapping(value = "/showRoles", method = RequestMethod.GET)
		@ResponseBody
		public List getAllTasks(HttpServletResponse res) {

			List roles = new ArrayList();
			JSONArray jsonArray = new JSONArray();
			try {
				roles = ucgcService.getAllData(); // getting roles using getAllData
			} catch (Exception e) {
				e.printStackTrace();
				res.setStatus(403);
			}
			return roles;
		}
		
		
//--------------------------------------- GovernanceConfigurationApprovalFlow---------------------------------
		
		@RequestMapping(value = "/listApprovalFlow/{id}", method = RequestMethod.GET)
		@ResponseBody
		public List getAllApprovalFlow(@PathVariable("id") byte approval_type, HttpServletResponse res) {

			GovernanceConfigurationApprovalFlow appFlow = new GovernanceConfigurationApprovalFlow();
			appFlow.setApprovalType(approval_type);
			List approvalFlowList = new ArrayList();
			try {
				approvalFlowList = ucgcService.getAllApprovalFlow(appFlow);
			} catch (Exception e) {
				e.printStackTrace();
				res.setStatus(403);
			}
			res.setStatus(200);
			return approvalFlowList;
		}

		// --------------------------------delete application flow (on gov_appl_id)----------------------

		@RequestMapping(value = "/deleteApprovalFlow/{id}", method = RequestMethod.DELETE)
		@ResponseBody
		public StatusMessage deleteApprovalFlow(@PathVariable("id") int gov_appl_id, HttpServletRequest req,
				HttpServletResponse res) {

			GovernanceConfigurationApprovalFlow approvalFlow = new GovernanceConfigurationApprovalFlow();
			approvalFlow.setGovApplId(gov_appl_id);
			StatusMessage msg = new StatusMessage();
			try {
				msg = ucgcService.deleteApprovalFlow(approvalFlow, req);
				if (msg.getStatusCode() == 406) {
					res.setStatus(406);
				} else {
					msg.setStatusCode(200);
					res.setStatus(200);
				}
			} catch (Exception e) {
				e.printStackTrace();
				res.setStatus(406);
				msg.setMessage("Approval flow deletion failed !!");
			}
			return msg;
		}

		
//---------------------------------------------Data Consumption--------------------------------
		@RequestMapping(value = "/data_consumption", method = RequestMethod.POST, headers = "content-type=application/json")
		@ResponseBody
		public StatusMessage dataConcumption(@RequestBody String dcon, HttpServletRequest req, HttpServletResponse res)
				throws IOException, JSONException, ParseException, java.text.ParseException {

			StatusMessage msg = new StatusMessage();
			int flag = 0;
			try {
				JSONParser parse = new JSONParser();
				org.json.simple.JSONObject jobj = (org.json.simple.JSONObject) parse.parse(dcon);
				org.json.simple.JSONArray data_outservice_array = (org.json.simple.JSONArray) jobj.get("data_consumption");
				System.out.println("++++"+data_outservice_array);
				for (int j = 0; j < data_outservice_array.size(); j++) {
					GovernanceConfigurationApprovalFlow approvalFlow = new GovernanceConfigurationApprovalFlow();
					org.json.simple.JSONObject data_outservice_obj = (org.json.simple.JSONObject) data_outservice_array
							.get(j);
					if (((Long) data_outservice_obj.get("gov_appl_id")).intValue() != 0) {
						flag = 0;
						// deleteing the rows of a perticular gov_app_id
						// GovernanceConfigurationApprovalFlow approvalFlowForDelete =
						// ucgcService.findByGovApplId(((Long)
						// data_outservice_obj.get("gov_appl_id")).intValue());
						GovernanceConfigurationApprovalFlow approvalFlowForDelete = new GovernanceConfigurationApprovalFlow();
						approvalFlowForDelete.setGovApplId(((Long) data_outservice_obj.get("gov_appl_id")).intValue());
						ucgcService.deleteApprovalFlow(approvalFlowForDelete, req);
						approvalFlow.setGovApplId(((Long) data_outservice_obj.get("gov_appl_id")).intValue());

						// updating updt_dt here
						
						DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
						String start_dt = formatter.format(new Date());
						Date date = (Date) formatter.parse(start_dt);
						approvalFlow.setUpdt_dt(date);
					} else {
						flag = 1;
						Integer maxId = ucgcService.getMaxId();
						if (maxId == null) {
							maxId = 0;
						}
						Integer maxVal = maxId;
						TableCountResult resultCount = new TableCountResult();
						resultCount.setRecord_count(maxVal.intValue());
						int gov_id = resultCount.getRecord_count();
						int NextfinalGovId = gov_id + 1;
						approvalFlow.setGovApplId(NextfinalGovId); // setting dummy value
					}
					try {
						if (data_outservice_obj.get("bdi_product_type") == null) {
							approvalFlow.setBdi_product_type(null);
						} else {
							approvalFlow
									.setBdi_product_type(((Long) data_outservice_obj.get("bdi_product_type")).byteValue());
						}
					} catch (Exception e) {
						System.out.println("pls provide bdi_product_type in JSON");
					}

					approvalFlow.setBu_id(null);
					approvalFlow.setBu_func_id(null);
					approvalFlow.setAccess_role_id(null);
					approvalFlow.setApprovalType((byte) 1);
					approvalFlow.setWith_sensitive_flg(((Long) data_outservice_obj.get("with_sensitive_flg")).byteValue());

					if (data_outservice_obj.get("data_sensitive_type") == null) {
						approvalFlow.setData_sensitive_type(null);
					} else {
						approvalFlow.setData_sensitive_type(
								((Long) data_outservice_obj.get("data_sensitive_type")).shortValue());
					}
					approvalFlow.setData_usage_types(((Long) data_outservice_obj.get("data_usage_types")).byteValue());

					approvalFlow.setPlatf_id((Long) data_outservice_obj.get("platf_id"));  //newly added
					//current timestamp
					Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
					approvalFlow.setUpdt_dt(currentTimestamp);

					org.json.simple.JSONArray level_array = (org.json.simple.JSONArray) data_outservice_obj.get("level");
					for (int k = 0; k < level_array.size(); k++) {
						org.json.simple.JSONObject level_obj = (org.json.simple.JSONObject) level_array.get(k);
						approvalFlow.setApproval_level(((Long) level_obj.get("approval_level")).byteValue());	
						
						/*approvalFlow.setApprover_type((Long) level_obj.get("approver_type"));    //newly added
						
						int approverType = ((Long) level_obj.get("approver_type")).intValue();
						
						if(approverType == 3)
						{
						approvalFlow.setOwner_type_name((String) level_obj.get("owner_type_name"));	
						}
						*/
						approvalFlow.setApprover1_nm((String) level_obj.get("approver1_nm"));
						approvalFlow.setApprover1_title((String) level_obj.get("approver1_title"));
						approvalFlow.setApprover1_email((String) level_obj.get("approver1_email"));
						approvalFlow.setApprover2_nm((String) level_obj.get("approver2_nm"));
						approvalFlow.setApprover2_title((String) level_obj.get("approver2_title"));
						approvalFlow.setApprover2_email((String) level_obj.get("approver2_email"));
						approvalFlow.setUpdt_by_id(((Long) level_obj.get("updt_by_id")).intValue());
						ucgcService.createApprovalFlow(approvalFlow, req);
					}
					if (((Long) data_outservice_obj.get("gov_appl_id")).intValue() == 0) {
						msg.setMessage("Data Consumption created successfully !!");
						msg.setStatusCode(200);
					} else {
						msg.setMessage("Data Consumption updated successfully !!");
						msg.setStatusCode(200);
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				res.setStatus(406);
				msg.setMessage("Data Consumption creation failed !");
			}
			return msg;
		}

		
		//---------------------------------------------Data service--------------------------------
				@RequestMapping(value = "/data_service", method = RequestMethod.POST, headers = "content-type=application/json")
				@ResponseBody
				public StatusMessage dataservice(@RequestBody String dser, HttpServletRequest req, HttpServletResponse res)
						throws IOException, JSONException, ParseException, java.text.ParseException {

					StatusMessage msg = new StatusMessage();
					int flag = 0;
					try {
						JSONParser parse = new JSONParser();
						org.json.simple.JSONObject jobj = (org.json.simple.JSONObject) parse.parse(dser);
						org.json.simple.JSONArray data_outservice_array = (org.json.simple.JSONArray) jobj.get("data_service");
						System.out.println("++++"+data_outservice_array);
						for (int j = 0; j < data_outservice_array.size(); j++) {
							GovernanceConfigurationApprovalFlow approvalFlow = new GovernanceConfigurationApprovalFlow();
							org.json.simple.JSONObject data_outservice_obj = (org.json.simple.JSONObject) data_outservice_array
									.get(j);
							if (((Long) data_outservice_obj.get("gov_appl_id")).intValue() != 0) {
								flag = 0;
					
								GovernanceConfigurationApprovalFlow approvalFlowForDelete = new GovernanceConfigurationApprovalFlow();
								approvalFlowForDelete.setGovApplId(((Long) data_outservice_obj.get("gov_appl_id")).intValue());
								ucgcService.deleteApprovalFlow(approvalFlowForDelete, req);
								approvalFlow.setGovApplId(((Long) data_outservice_obj.get("gov_appl_id")).intValue());

								// updating updt_dt here
								
								DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
								String start_dt = formatter.format(new Date());
								Date date = (Date) formatter.parse(start_dt);
								approvalFlow.setUpdt_dt(date);
							} else {
								flag = 1;
								Integer maxId = ucgcService.getMaxId();
								if (maxId == null) {
									maxId = 0;
								}
								Integer maxVal = maxId;
								TableCountResult resultCount = new TableCountResult();
								resultCount.setRecord_count(maxVal.intValue());
								int gov_id = resultCount.getRecord_count();
								int NextfinalGovId = gov_id + 1;
								approvalFlow.setGovApplId(NextfinalGovId); // setting dummy value
							}
							try {
								if (data_outservice_obj.get("bdi_product_type") == null) {
									approvalFlow.setBdi_product_type(null);
								} else {
									approvalFlow
											.setBdi_product_type(((Long) data_outservice_obj.get("bdi_product_type")).byteValue());
								}
							} catch (Exception e) {
								System.out.println("pls provide bdi_product_type in JSON");
							}

							approvalFlow.setBu_id(null);
							approvalFlow.setBu_func_id(null);
							approvalFlow.setAccess_role_id(null);
							approvalFlow.setApprovalType((byte) 3);
							approvalFlow.setWith_sensitive_flg(((Long) data_outservice_obj.get("with_sensitive_flg")).byteValue());

							if (data_outservice_obj.get("data_sensitive_type") == null) {
								approvalFlow.setData_sensitive_type(null);
							} else {
								approvalFlow.setData_sensitive_type(
										((Long) data_outservice_obj.get("data_sensitive_type")).shortValue());
							}
							approvalFlow.setData_usage_types(((Long) data_outservice_obj.get("data_usage_types")).byteValue());

							approvalFlow.setPlatf_id((Long) data_outservice_obj.get("platf_id"));  //newly added
							//current timestamp
							Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
							approvalFlow.setUpdt_dt(currentTimestamp);

							org.json.simple.JSONArray level_array = (org.json.simple.JSONArray) data_outservice_obj.get("level");
							for (int k = 0; k < level_array.size(); k++) {
								org.json.simple.JSONObject level_obj = (org.json.simple.JSONObject) level_array.get(k);
								approvalFlow.setApproval_level(((Long) level_obj.get("approval_level")).byteValue());
								
								/*approvalFlow.setApprover_type((Long) level_obj.get("approver_type"));    //newly added
								
								int approverType = ((Long) level_obj.get("approver_type")).intValue();
								
								if(approverType == 3)
								{
								approvalFlow.setOwner_type_name((String) level_obj.get("owner_type_name"));	
								}
								*/
								approvalFlow.setApprover1_nm((String) level_obj.get("approver1_nm"));
								approvalFlow.setApprover1_title((String) level_obj.get("approver1_title"));
								approvalFlow.setApprover1_email((String) level_obj.get("approver1_email"));
								approvalFlow.setApprover2_nm((String) level_obj.get("approver2_nm"));
								approvalFlow.setApprover2_title((String) level_obj.get("approver2_title"));
								approvalFlow.setApprover2_email((String) level_obj.get("approver2_email"));
								approvalFlow.setUpdt_by_id(((Long) level_obj.get("updt_by_id")).intValue());
								ucgcService.createApprovalFlow(approvalFlow, req);
							}
							if (((Long) data_outservice_obj.get("gov_appl_id")).intValue() == 0) {
								msg.setMessage("Data service created successfully !!");
								msg.setStatusCode(200);
							} else {
								msg.setMessage("Data service updated successfully !!");
								msg.setStatusCode(200);
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						res.setStatus(406);
						msg.setMessage("Data service creation failed !");
					}
					return msg;
				}

				
				
				//---------------------------------------------Data TRANSFORMATION--------------------------------
						@RequestMapping(value = "/data_transformation", method = RequestMethod.POST, headers = "content-type=application/json")
						@ResponseBody
						public StatusMessage datatransformation(@RequestBody String dtra, HttpServletRequest req, HttpServletResponse res)
								throws IOException, JSONException, ParseException, java.text.ParseException {

							StatusMessage msg = new StatusMessage();
							int flag = 0;
							try {
								JSONParser parse = new JSONParser();
								org.json.simple.JSONObject jobj = (org.json.simple.JSONObject) parse.parse(dtra);
								org.json.simple.JSONArray data_outservice_array = (org.json.simple.JSONArray) jobj.get("data_transformation");
								System.out.println("++++"+data_outservice_array);
								for (int j = 0; j < data_outservice_array.size(); j++) {
									GovernanceConfigurationApprovalFlow approvalFlow = new GovernanceConfigurationApprovalFlow();
									org.json.simple.JSONObject data_outservice_obj = (org.json.simple.JSONObject) data_outservice_array
											.get(j);
									if (((Long) data_outservice_obj.get("gov_appl_id")).intValue() != 0) {
										flag = 0;
										// deleteing the rows of a perticular gov_app_id
										// GovernanceConfigurationApprovalFlow approvalFlowForDelete =
										// ucgcService.findByGovApplId(((Long)
										// data_outservice_obj.get("gov_appl_id")).intValue());
										GovernanceConfigurationApprovalFlow approvalFlowForDelete = new GovernanceConfigurationApprovalFlow();
										approvalFlowForDelete.setGovApplId(((Long) data_outservice_obj.get("gov_appl_id")).intValue());
										ucgcService.deleteApprovalFlow(approvalFlowForDelete, req);
										approvalFlow.setGovApplId(((Long) data_outservice_obj.get("gov_appl_id")).intValue());

										// updating updt_dt here
										
										DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
										String start_dt = formatter.format(new Date());
										Date date = (Date) formatter.parse(start_dt);
										approvalFlow.setUpdt_dt(date);
									} else {
										flag = 1;
										Integer maxId = ucgcService.getMaxId();
										if (maxId == null) {
											maxId = 0;
										}
										Integer maxVal = maxId;
										TableCountResult resultCount = new TableCountResult();
										resultCount.setRecord_count(maxVal.intValue());
										int gov_id = resultCount.getRecord_count();
										int NextfinalGovId = gov_id + 1;
										approvalFlow.setGovApplId(NextfinalGovId); // setting dummy value
									}
									try {
										if (data_outservice_obj.get("bdi_product_type") == null) {
											approvalFlow.setBdi_product_type(null);
										} else {
											approvalFlow
													.setBdi_product_type(((Long) data_outservice_obj.get("bdi_product_type")).byteValue());
										}
									} catch (Exception e) {
										System.out.println("pls provide bdi_product_type in JSON");
									}

									approvalFlow.setBu_id(null);
									approvalFlow.setBu_func_id(null);
									approvalFlow.setAccess_role_id(null);
									approvalFlow.setApprovalType((byte) 2);
									approvalFlow.setWith_sensitive_flg(((Long) data_outservice_obj.get("with_sensitive_flg")).byteValue());

									if (data_outservice_obj.get("data_sensitive_type") == null) {
										approvalFlow.setData_sensitive_type(null);
									} else {
										approvalFlow.setData_sensitive_type(
												((Long) data_outservice_obj.get("data_sensitive_type")).shortValue());
									}
									approvalFlow.setData_usage_types(((Long) data_outservice_obj.get("data_usage_types")).byteValue());

									approvalFlow.setPlatf_id((Long) data_outservice_obj.get("platf_id"));  //newly added
									//current timestamp
									Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
									approvalFlow.setUpdt_dt(currentTimestamp);

									org.json.simple.JSONArray level_array = (org.json.simple.JSONArray) data_outservice_obj.get("level");
									for (int k = 0; k < level_array.size(); k++) {
										org.json.simple.JSONObject level_obj = (org.json.simple.JSONObject) level_array.get(k);
										approvalFlow.setApproval_level(((Long) level_obj.get("approval_level")).byteValue());
										
										/*approvalFlow.setApprover_type((Long) level_obj.get("approver_type"));    //newly added
										
										int approverType = ((Long) level_obj.get("approver_type")).intValue();
										
										if(approverType == 3)
										{
										approvalFlow.setOwner_type_name((String) level_obj.get("owner_type_name"));	
										}
										*/
										
										approvalFlow.setApprover1_nm((String) level_obj.get("approver1_nm"));
										approvalFlow.setApprover1_title((String) level_obj.get("approver1_title"));
										approvalFlow.setApprover1_email((String) level_obj.get("approver1_email"));
										approvalFlow.setApprover2_nm((String) level_obj.get("approver2_nm"));
										approvalFlow.setApprover2_title((String) level_obj.get("approver2_title"));
										approvalFlow.setApprover2_email((String) level_obj.get("approver2_email"));
										approvalFlow.setUpdt_by_id(((Long) level_obj.get("updt_by_id")).intValue());
										ucgcService.createApprovalFlow(approvalFlow, req);
									}
									if (((Long) data_outservice_obj.get("gov_appl_id")).intValue() == 0) {
										msg.setMessage("Data transformation created successfully !!");
										msg.setStatusCode(200);
									} else {
										msg.setMessage("Data transformation updated successfully !!");
										msg.setStatusCode(200);
									}
								}

							} catch (Exception e) {
								e.printStackTrace();
								res.setStatus(406);
								msg.setMessage("Data transformation creation failed !");
							}
							return msg;
						}


						// dropdown (menu group and menu name)
						@RequestMapping(value = "/listMenuDetails/{bdi_product_type}", method = RequestMethod.GET)
						@ResponseBody
						public List getAllUsers(@PathVariable("bdi_product_type") int bdi_product_type, HttpServletResponse res) {

							List userList = new ArrayList();
							try {
								userList = ucgcService.getAllApp(bdi_product_type);
							} catch (Exception e) {
								e.printStackTrace();
								res.setStatus(403);
							}
							res.setStatus(200);
							return userList;
						}
						
					
					}


