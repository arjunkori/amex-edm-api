package com.bdiplus.edmapi.dto;

import java.util.List;

public class StatusMessage<T> {
	
	private int StatusCode;
	private String message;
	//List<T> UserApprovedRoles;

	List <T> listData;
	T data;
	public int getStatusCode() {
		return StatusCode;
	}
	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<T> getListData() {
		return listData;
	}
	public void setListData(List<T> listData) {
		this.listData = listData;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
//	public List<T> getUserApprovedRoles() {
//		return UserApprovedRoles;
//	}
//	public void setUserApprovedRoles(List<T> userApprovedRoles) {
//		UserApprovedRoles = userApprovedRoles;
//	}
	

}
