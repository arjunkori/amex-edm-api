package com.bdiplus.edmapi.dto;

public class TableCountResult {

	private Integer record_count;

	public Integer getRecord_count() {
		return record_count;
	}

	public void setRecord_count(Integer record_count) {
		this.record_count = record_count;
	}
}
