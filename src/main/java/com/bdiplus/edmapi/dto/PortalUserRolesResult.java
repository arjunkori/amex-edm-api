package com.bdiplus.edmapi.dto;

public class PortalUserRolesResult {
	private short bdiapp_role;
	private String bdiapp_role_nm;
	public short getBdiapp_role() {
		return bdiapp_role;
	}
	public void setBdiapp_role(short bdiapp_role) {
		this.bdiapp_role = bdiapp_role;
	}
	public String getBdiapp_role_nm() {
		return bdiapp_role_nm;
	}
	public void setBdiapp_role_nm(String bdiapp_role_nm) {
		this.bdiapp_role_nm = bdiapp_role_nm;
	}
	
}
